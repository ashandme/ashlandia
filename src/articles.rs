#[derive(Serialize, Deserialize, Debug)]
pub struct Articles {
    pub articles: Vec<Article>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Article {
    pub name: String,
    pub path: String,
    pub wpath: String,
    pub date: String,
}

impl Article {
	pub fn new() -> Article {
		let a = Article {
			name: String::new(),
			path: String::new(),
			wpath: String::new(),
			date: String::new(),
		};
		a
	}
}
