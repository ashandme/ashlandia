#[macro_use]
extern crate horrorshow;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;

use actix_files::NamedFile;
use actix_web::{web, App, HttpRequest, HttpServer, Result};

use diesel::{
    r2d2::{self, ConnectionManager},
    SqliteConnection,
};

use std::path::PathBuf;
pub type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

mod articles;
mod routes;
mod models;
mod schema;

static IP: &str = "127.0.0.1";
const PORT: u16 = 8080;

async fn index(req: HttpRequest) -> Result<NamedFile> {
    let path: PathBuf = req.match_info().query("filename").parse().unwrap();
    Ok(NamedFile::open(path)?)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("environment DATABASE_URL not found");
    let database_pool = Pool::builder()
        .build(ConnectionManager::<SqliteConnection>::new(database_url))
        .unwrap();
    eprintln!("started http at http://{}:{}", IP, PORT);
    HttpServer::new(move || {
        App::new()
			.data(database_pool.clone())
            .route("/", web::get().to(routes::home))
            .route("/article/{s}", web::get().to(routes::show_article))
            .route("/public/{filename:.*}", web::get().to(index))
            .route("/blog", web::get().to(routes::blog))
            .route("/post", web::post().to(routes::add_post))
    })
    .bind((IP, PORT))?
    .run()
    .await
}
