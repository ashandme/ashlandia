use serde::{ Deserialize, Serialize};
use crate::schema::*;

#[derive(Debug, Serialize, Deserialize, Queryable)]
pub struct Post {
	pub id: i32,
	pub reference: String,
	pub caption: String,
	pub date_create: String,
}

#[derive(Debug, Insertable)] #[table_name = "posts"]
pub struct PostNew<'a> {
	pub reference: &'a str,
	pub caption: &'a str,
	pub date_create: &'a str,
}

#[derive(Serialize, Deserialize)]
pub struct PostForm {
	pub reference: String,
	pub caption: String,
}
