-- Your SQL goes here
CREATE TABLE posts (
  id INTEGER NOT NULL PRIMARY KEY, 
  reference TINYTEXT NOT NULL,
  caption TEXT NOT NULL,
  date_create TEXT NOT NULL
)
