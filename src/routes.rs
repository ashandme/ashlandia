extern crate serde;
extern crate serde_json;

use actix_web::{http::StatusCode, Error, HttpRequest, HttpResponse, web};
use horrorshow::helper::doctype;
use horrorshow::prelude::*;
use std::fs;

use anyhow::Result;
use diesel::{dsl::insert_into, prelude::*, ExpressionMethods, RunQueryDsl};

use crate::articles::{Article, Articles};
use crate::models::{Post, PostForm, PostNew};
use crate::Pool;

fn get_articles() -> Vec<Article> {
    let json = read_file("./static/articles.json".to_string());
    let res = serde_json::from_str(json.as_str());
    if res.is_err() {
        panic!("something bad happend");
    }
    let p: Articles = res.unwrap();
    p.articles
}

fn read_file(f: String) -> String {
    let data = fs::read_to_string(f.as_str()).expect("unable to read file");
    data
}

fn tempg(a: Option<Article>, c: Option<Vec<Post>>, f: String) -> String {
    let actual = format!(
        "{}",
        html! {
            : doctype::HTML;
            html {
                head {
                    : Raw(read_file("./static/head.html"
                                    .to_string()));
                    style {
                        : Raw(read_file("./css/style.css"
                                        .to_string()));
                    }
                }
                body {
                    : Raw(read_file("./static/nav.html"
                                    .to_string()));
                    : Raw(f.as_str());
					@if let Some(article) = a.clone() {
						@if let Some(posts) = &c {
							@for i in posts.iter() {
								@if i.reference.eq(&article.name) {
									div(id=format!("p{}", i.id), class="comment"){
                                        div(class="post_info"){
										    span(class="id"): format!("#{}",i.id);
                                            : Raw(" ");
                                            span(class="date_create"): i.date_create.as_str();
                                        }
										blockquote(class="caption"): i.caption.as_str();
									}
								}
							}
						}
                        form(id="comment_section" ,action="/post", method="POST") {
							input(name="reference", type="hidden", value=article.name.as_str());
                            p(class="comment_textarea"){
							label(for="caption") {
								: "Caption: ";
							}
							    textarea(rows="3", cols="50", name="caption", form="comment_section");
                            }
							button (type="submit"){
								: "Post Comment";
							}
						}
					}
                    : Raw(read_file("./static/footer.html"
                                    .to_string()));
                }
            }
        }
    );
    actual
}
    
pub async fn home() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::build(StatusCode::OK)
       .content_type("text/html; charset=utf-8")
	   .body(tempg(None, None, read_file("./static/index.html".to_string()))))
}

pub async fn blog() -> Result<HttpResponse, Error> {
    let indx = format!(
        "{}",
        html! {
            article {
                ol {
                    @ for i in get_articles() {
                        li(class="item") {
                            a(href = format!("/article/{}",i.wpath)){
                                : Raw(i.name);
                            }
                            span (class="date")  {
                                : i.date.as_str();
                            }
                        }
                    }
                }
            }
        }
    );
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(tempg(None , None,indx.to_string())))
}
pub async fn show_article(pool: web::Data<Pool>, h: HttpRequest) -> HttpResponse {
    let d = h.match_info().get("s").unwrap();
	let mut a: Article = Article::new();
    let dbresult = get_all_posts(pool).await;
    for i in get_articles() {
        if i.wpath.eq(d) {
            a = i;
        }
    }
    HttpResponse::Ok()
        .content_type("text/html")
        .body(tempg(Some(a.clone()),Some(dbresult.unwrap()),read_file(a.path)))
}

fn add_single_post(
    pool: web::Data<Pool>,
    item: web::Form<PostForm>,
) -> Result<Post, diesel::result::Error> {
    use crate::schema::posts::dsl::*;
    let db_connection = pool.get().unwrap();
    match posts
		.filter(caption.eq(&item.caption))
        .get_result::<Post>(&db_connection)
    {
        Ok(result) => Ok(result),
        Err(_) => {
            let new_post = PostNew {
                reference: &item.reference,
                caption: &item.caption,
                date_create: &format!("{}", chrono::Local::now().naive_local()),
            };
            insert_into(posts)
                .values(&new_post)
                .execute(&db_connection)
                .expect("Error saving the post");

            let result = posts.order(id.desc()).first(&db_connection).unwrap();
            Ok(result)
        }
    }
}

pub async fn add_post(
    pool: web::Data<Pool>,
    item: web::Form<PostForm>,
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || add_single_post(pool, item))
        .await
        .map(|_post| HttpResponse::Created().take().finish())
        .map_err(|_| HttpResponse::InternalServerError())?)
}

async fn get_all_posts(pool: web::Data<Pool>
)-> Result<Vec<Post>, diesel::result::Error>{
	use crate::schema::posts::dsl::*;
	let db_connection = pool.get().unwrap();
	let result = posts.load::<Post>(&db_connection)?;
	Ok(result)
}
